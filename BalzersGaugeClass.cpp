/*----- PROTECTED REGION ID(BalzersGaugeClass.cpp) ENABLED START -----*/
static const char *RcsId      = "$Id: BalzersGaugeClass.cpp,v 1.11 2011/09/19 10:55:00 pascal_verdier Exp $";
static const char *TagName    = "$Name: BalzersGauge-Release_3_0 $";
static const char *CvsPath    = "$Source: /cvsroot/tango-ds/Vacuum/BalzersGauge/BalzersGaugeClass.cpp,v $";
static const char *SvnPath    = "$HeadURL:  $";
static const char *HttpServer = "http://www.esrf.eu/computing/cs/tango/tango_doc/ds_doc/";
//=============================================================================
//
// file :        BalzersGaugeClass.cpp
//
// description : C++ source for the BalzersGaugeClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the �name� once per process.
//
// project :     Balzers gauge device server..
//
// $Author: pascal_verdier $
//
// $Revision: 1.11 $
// $Date: 2011/09/19 10:55:00 $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source: /cvsroot/tango-ds/Vacuum/BalzersGauge/BalzersGaugeClass.cpp,v $
// $Log: BalzersGaugeClass.cpp,v $
// Revision 1.11  2011/09/19 10:55:00  pascal_verdier
// Relays are now managed in TPG class (not by gauge).
//
// Revision 1.10  2011/05/31 06:00:48  pascal_verdier
// Cosmetic changes.
//
// Revision 1.9  2011/01/19 15:11:27  pascal_verdier
// Pb in state/status during initialization fixed.
//
// Revision 1.8  2010/10/13 12:51:18  pascal_verdier
// Pogo-7 compatibility
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <tango.h>
#include <BalzersGauge.h>
#include <BalzersGaugeClass.h>

/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass.cpp

//-------------------------------------------------------------------
/**
 *	Create BalzersGaugeClass singleton and
 *	return it in a C function for Python usage
 */
//-------------------------------------------------------------------
extern "C" {
#ifdef _TG_WINDOWS_

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_BalzersGauge_class(const char *name) {
		return BalzersGauge_ns::BalzersGaugeClass::init(name);
	}
}

namespace BalzersGauge_ns
{
//===================================================================
//	Initialize pointer for singleton pattern
//===================================================================
BalzersGaugeClass *BalzersGaugeClass::_instance = NULL;

//===================================================================
//	Class constants
//===================================================================
//--------------------------------------------------------
/**
 * method : 		BalzersGaugeClass::BalzersGaugeClass(std::string &s)
 * description : 	constructor for the BalzersGaugeClass
 *
 * @param s	The class name
 */
//--------------------------------------------------------
BalzersGaugeClass::BalzersGaugeClass(std::string &s):PressureGauge_ns::PressureGaugeClass(s)
{
	TANGO_LOG_INFO << "Entering BalzersGaugeClass constructor" << std::endl;
	set_default_property();
	write_class_property();

	/*----- PROTECTED REGION ID(BalzersGaugeClass::constructor) ENABLED START -----*/

	

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::constructor

	TANGO_LOG_INFO << "Leaving BalzersGaugeClass constructor" << std::endl;
}

//--------------------------------------------------------
/**
 * method : 		BalzersGaugeClass::~BalzersGaugeClass()
 * description : 	destructor for the BalzersGaugeClass
 */
//--------------------------------------------------------
BalzersGaugeClass::~BalzersGaugeClass()
{
	/*----- PROTECTED REGION ID(BalzersGaugeClass::destructor) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::destructor

	_instance = NULL;
}


//--------------------------------------------------------
/**
 * method : 		BalzersGaugeClass::init
 * description : 	Create the object if not already done.
 *                  Otherwise, just return a pointer to the object
 *
 * @param	name	The class name
 */
//--------------------------------------------------------
BalzersGaugeClass *BalzersGaugeClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			std::string s(name);
			_instance = new BalzersGaugeClass(s);
		}
		catch (std::bad_alloc &)
		{
			throw;
		}
	}
	return _instance;
}

//--------------------------------------------------------
/**
 * method : 		BalzersGaugeClass::instance
 * description : 	Check if object already created,
 *                  and return a pointer to the object
 */
//--------------------------------------------------------
BalzersGaugeClass *BalzersGaugeClass::instance()
{
	if (_instance == NULL)
	{
		std::cerr << "Class is not initialized !!" << std::endl;
		exit(-1);
	}
	return _instance;
}



//===================================================================
//	Command execution method calls
//===================================================================
//--------------------------------------------------------
/**
 * method : 		OnClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OnClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "OnClass::execute(): arrived" << std::endl;
	((static_cast<BalzersGauge *>(device))->on());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		OffClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OffClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "OffClass::execute(): arrived" << std::endl;
	((static_cast<BalzersGauge *>(device))->off());
	return new CORBA::Any();
}


//===================================================================
//	Properties management
//===================================================================
//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::get_class_property()
 *	Description: Get the class property for specified name.
 */
//--------------------------------------------------------
Tango::DbDatum BalzersGaugeClass::get_class_property(std::string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, returns  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::get_default_device_property()
 *	Description: Return the default value for device property.
 */
//--------------------------------------------------------
Tango::DbDatum BalzersGaugeClass::get_default_device_property(std::string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::get_default_class_property()
 *	Description: Return the default value for class property.
 */
//--------------------------------------------------------
Tango::DbDatum BalzersGaugeClass::get_default_class_property(std::string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}


//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::set_default_property()
 *	Description: Set default property (class and device) for wizard.
 *                For each property, add to wizard property name and description.
 *                If default value has been set, add it to wizard property and
 *                store it in a DbDatum.
 */
//--------------------------------------------------------
void BalzersGaugeClass::set_default_property()
{
	std::string	prop_name;
	std::string	prop_desc;
	std::string	prop_def;
	std::vector<std::string>	vect_data;

	//	Set Default Class Properties

	//	Set Default device Properties
	prop_name = "Tpg300DeviceName";
	prop_desc = "The device name of the TPG300 device server.";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "GaugeName";
	prop_desc = "This is the gauge name (must be \'A1\', \'A2\', \'B1\' or \'B2\').";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "FilterTime";
	prop_desc = "The type of filtering (\'fast\', \'slow\' or \'medium\').";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "XLocation";
	prop_desc = "Tha abscisse of the gauge inside the cell.";
	prop_def  = "NaN";
	vect_data.clear();
	vect_data.push_back("NaN");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "YLocation";
	prop_desc = "The Y coordinate of the gauge in the cell.";
	prop_def  = "NaN";
	vect_data.clear();
	vect_data.push_back("NaN");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
}

//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::write_class_property()
 *	Description: Set class description fields as property in database
 */
//--------------------------------------------------------
void BalzersGaugeClass::write_class_property()
{
	//	First time, check if database used
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	std::string	classname = get_name();
	std::string	header;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	std::string	str_title("Balzers gauge device server.");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	std::vector<std::string>	str_desc;
	str_desc.push_back("This is device server is intended to drive a vacuum gauge connected");
	str_desc.push_back("to a Balzers TPG300 controller.");
	description << str_desc;
	data.push_back(description);

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	std::vector<std::string> inheritance;
	inheritance.push_back("TANGO_BASE_CLASS");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	get_db_class()->put_property(data);
}

//===================================================================
//	Factory methods
//===================================================================

//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::device_factory()
 *	Description: Create the device object(s)
 *                and store them in the device list
 */
//--------------------------------------------------------
void BalzersGaugeClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{
	/*----- PROTECTED REGION ID(BalzersGaugeClass::device_factory_before) ENABLED START -----*/

	

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::device_factory_before

	//	Create devices and add it into the device list
	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		TANGO_LOG_DEBUG << "Device name : " << (*devlist_ptr)[i].in() << std::endl;
		device_list.push_back(new BalzersGauge(this, (*devlist_ptr)[i]));
	}

	//	Manage dynamic attributes if any
	erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

	//	Export devices to the outside world
	for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
	{
		//	Add dynamic attributes if any
		BalzersGauge *dev = static_cast<BalzersGauge *>(device_list[device_list.size()-i]);
		dev->add_dynamic_attributes();

		//	Check before if database used.
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(dev);
		else
			export_device(dev, dev->get_name().c_str());
	}

	/*----- PROTECTED REGION ID(BalzersGaugeClass::device_factory_after) ENABLED START -----*/

	

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::device_factory_after
}
//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::attribute_factory()
 *	Description: Create the attribute object(s)
 *                and store them in the attribute list
 */
//--------------------------------------------------------
void BalzersGaugeClass::attribute_factory(std::vector<Tango::Attr *> &att_list)
{
	/*----- PROTECTED REGION ID(BalzersGaugeClass::attribute_factory_before) ENABLED START -----*/

	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::attribute_factory_before
	//	Call atribute_factory for inherited class
	PressureGauge_ns::PressureGaugeClass::attribute_factory(att_list);

	//	Attribute : Pressure - Check if not concrete in inherited class
	Tango::Attr *PressureAttr = get_attr_object_by_name(att_list, "Pressure");
	if (PressureAttr == NULL)
	{
		//	Attribute : Pressure
		PressureAttrib	*pressure = new PressureAttrib();
		Tango::UserDefaultAttrProp	pressure_prop;
		pressure_prop.set_description("Measured pressure on device.");
		pressure_prop.set_label("Pressure");
		pressure_prop.set_unit("mBar");
		//	standard_unit	not set for Pressure
		//	display_unit	not set for Pressure
		pressure_prop.set_format("%.1e");
		//	max_value	not set for Pressure
		//	min_value	not set for Pressure
		//	max_alarm	not set for Pressure
		//	min_alarm	not set for Pressure
		//	max_warning	not set for Pressure
		//	min_warning	not set for Pressure
		//	delta_t	not set for Pressure
		//	delta_val	not set for Pressure
		pressure->set_default_properties(pressure_prop);
		//	Not Polled
		pressure->set_disp_level(Tango::OPERATOR);
		//	Not Memorized
		att_list.push_back(pressure);
	}

	//	Attribute : XLocation - Check if not concrete in inherited class
	Tango::Attr *XLocationAttr = get_attr_object_by_name(att_list, "XLocation");
	if (XLocationAttr == NULL)
	{
		//	Attribute : XLocation
		XLocationAttrib	*xlocation = new XLocationAttrib();
		Tango::UserDefaultAttrProp	xlocation_prop;
		//	description	not set for XLocation
		//	label	not set for XLocation
		//	unit	not set for XLocation
		//	standard_unit	not set for XLocation
		//	display_unit	not set for XLocation
		//	format	not set for XLocation
		//	max_value	not set for XLocation
		//	min_value	not set for XLocation
		//	max_alarm	not set for XLocation
		//	min_alarm	not set for XLocation
		//	max_warning	not set for XLocation
		//	min_warning	not set for XLocation
		//	delta_t	not set for XLocation
		//	delta_val	not set for XLocation
		xlocation->set_default_properties(xlocation_prop);
		//	Not Polled
		xlocation->set_disp_level(Tango::OPERATOR);
		//	Not Memorized
		att_list.push_back(xlocation);
	}

	//	Attribute : YLocation - Check if not concrete in inherited class
	Tango::Attr *YLocationAttr = get_attr_object_by_name(att_list, "YLocation");
	if (YLocationAttr == NULL)
	{
		//	Attribute : YLocation
		YLocationAttrib	*ylocation = new YLocationAttrib();
		Tango::UserDefaultAttrProp	ylocation_prop;
		//	description	not set for YLocation
		//	label	not set for YLocation
		//	unit	not set for YLocation
		//	standard_unit	not set for YLocation
		//	display_unit	not set for YLocation
		//	format	not set for YLocation
		//	max_value	not set for YLocation
		//	min_value	not set for YLocation
		//	max_alarm	not set for YLocation
		//	min_alarm	not set for YLocation
		//	max_warning	not set for YLocation
		//	min_warning	not set for YLocation
		//	delta_t	not set for YLocation
		//	delta_val	not set for YLocation
		ylocation->set_default_properties(ylocation_prop);
		//	Not Polled
		ylocation->set_disp_level(Tango::OPERATOR);
		//	Not Memorized
		att_list.push_back(ylocation);
	}


	//	Create a list of static attributes
	create_static_attribute_list(get_class_attr()->get_attr_list());
	/*----- PROTECTED REGION ID(BalzersGaugeClass::attribute_factory_after) ENABLED START -----*/

	//	Add your own code

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::attribute_factory_after
}
//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::pipe_factory()
 *	Description: Create the pipe object(s)
 *                and store them in the pipe list
 */
//--------------------------------------------------------
void BalzersGaugeClass::pipe_factory()
{
	/*----- PROTECTED REGION ID(BalzersGaugeClass::pipe_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::pipe_factory_before
	/*----- PROTECTED REGION ID(BalzersGaugeClass::pipe_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::pipe_factory_after
}
//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::command_factory()
 *	Description: Create the command object(s)
 *                and store them in the command list
 */
//--------------------------------------------------------
void BalzersGaugeClass::command_factory()
{
	/*----- PROTECTED REGION ID(BalzersGaugeClass::command_factory_before) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::command_factory_before
	//	Call command_factory for inherited class
	PressureGauge_ns::PressureGaugeClass::command_factory();


	//	Set polling perod for command State
	Tango::Command	&stateCmd = get_cmd_by_name("State");
	stateCmd.set_polling_period(1000);
	

	//	Get inherited Command object On if already created
	try
	{
		get_cmd_by_name("On");
	}
	catch (Tango::DevFailed &e)
	{
		//	Create On command object
		OnClass	*pOnCmd =
			new OnClass("On",
				Tango::DEV_VOID, Tango::DEV_VOID,
				"",
				"",
				Tango::OPERATOR);
		command_list.push_back(pOnCmd);
	}

	//	Get inherited Command object Off if already created
	try
	{
		get_cmd_by_name("Off");
	}
	catch (Tango::DevFailed &e)
	{
		//	Create Off command object
		OffClass	*pOffCmd =
			new OffClass("Off",
				Tango::DEV_VOID, Tango::DEV_VOID,
				"",
				"",
				Tango::OPERATOR);
		command_list.push_back(pOffCmd);
	}

	/*----- PROTECTED REGION ID(BalzersGaugeClass::command_factory_after) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::command_factory_after
}

//===================================================================
//	Dynamic attributes related methods
//===================================================================

//--------------------------------------------------------
/**
 * method : 		BalzersGaugeClass::create_static_attribute_list
 * description : 	Create the a list of static attributes
 *
 * @param	att_list	the created attribute list
 */
//--------------------------------------------------------
void BalzersGaugeClass::create_static_attribute_list(std::vector<Tango::Attr *> &att_list)
{
	for (unsigned long i=0 ; i<att_list.size() ; i++)
	{
		std::string att_name(att_list[i]->get_name());
		transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
		defaultAttList.push_back(att_name);
	}

	TANGO_LOG_INFO << defaultAttList.size() << " attributes in default list" << std::endl;

	/*----- PROTECTED REGION ID(BalzersGaugeClass::create_static_att_list) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::create_static_att_list
}


//--------------------------------------------------------
/**
 * method : 		BalzersGaugeClass::erase_dynamic_attributes
 * description : 	delete the dynamic attributes if any.
 *
 * @param	devlist_ptr	the device list pointer
 * @param	list of all attributes
 */
//--------------------------------------------------------
void BalzersGaugeClass::erase_dynamic_attributes(const Tango::DevVarStringArray *devlist_ptr, std::vector<Tango::Attr *> &att_list)
{
	Tango::Util *tg = Tango::Util::instance();

	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		Tango::DeviceImpl *dev_impl = tg->get_device_by_name(((std::string)(*devlist_ptr)[i]).c_str());
		BalzersGauge *dev = static_cast<BalzersGauge *> (dev_impl);

		std::vector<Tango::Attribute *> &dev_att_list = dev->get_device_attr()->get_attribute_list();
		std::vector<Tango::Attribute *>::iterator ite_att;
		for (ite_att=dev_att_list.begin() ; ite_att != dev_att_list.end() ; ++ite_att)
		{
			std::string att_name((*ite_att)->get_name_lower());
			if ((att_name == "state") || (att_name == "status"))
				continue;
			std::vector<std::string>::iterator ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
			if (ite_str == defaultAttList.end())
			{
				TANGO_LOG_INFO << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i] << std::endl;
				Tango::Attribute &att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
				dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
				--ite_att;
			}
		}
	}
	/*----- PROTECTED REGION ID(BalzersGaugeClass::erase_dynamic_attributes) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::erase_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method     : BalzersGaugeClass::get_attr_object_by_name()
 *	Description: returns Tango::Attr * object found by name
 */
//--------------------------------------------------------
Tango::Attr *BalzersGaugeClass::get_attr_object_by_name(std::vector<Tango::Attr *> &att_list, std::string attname)
{
	std::vector<Tango::Attr *>::iterator it;
	for (it=att_list.begin() ; it<att_list.end() ; ++it)
		if ((*it)->get_name()==attname)
			return (*it);
	//	Attr does not exist
	return NULL;
}


/*----- PROTECTED REGION ID(BalzersGaugeClass::Additional Methods) ENABLED START -----*/

	/*----- PROTECTED REGION END -----*/	//	BalzersGaugeClass::Additional Methods
} //	namespace
