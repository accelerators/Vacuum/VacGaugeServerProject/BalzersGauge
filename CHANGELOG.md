# Changelog


#### BalzersGauge-1.4 - 08/08/24:
    remove state machine for reading presure


#### BalzersGauge-1.3 - 24/05/23:

    allow pressure reading in ALARM state

#### BalzersGauge-1.2 - 04/08/21:

    improved logging

#### BalzersGauge-1.1 - 04/03/19:
    As request by vacuum group, GazFactor as been removed

#### BalzersGauge-1.0 - 04/03/19:
    Initial Revision in GIT
